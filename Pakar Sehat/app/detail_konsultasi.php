<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail_konsultasi extends Model
{
    protected $table = 'detail_konsultasis';
    protected $fillable = ['id_konsultasi', 'orang_konsultasi', 'isi_konsultasi', 'waktu_konsultasi', 'title'];
    public $timestamps = false;
    public function konsultasi(){
        return $this->belongsTo('App\konsultasi', 'id_konsultasi', 'id');
    }
}
