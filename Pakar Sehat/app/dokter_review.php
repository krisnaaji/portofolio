<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dokter_review extends Model
{
    public $table = "dokter_reviews";
    public function review()
    {
        return $this->belongsTo('App\Dokter', 'id', 'id_dokter');
    }
}
