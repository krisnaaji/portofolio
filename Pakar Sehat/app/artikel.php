<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class artikel extends Model
{
    protected $table = 'artikels';
    protected $fillable = ['judul_artikel', 'gambar_artikel', 'detail_artikel'];

    public function kategori_artikel(){
        return $this->belongsTo('App\Kategori_artikel','kategori_id', 'id');
    }
}
