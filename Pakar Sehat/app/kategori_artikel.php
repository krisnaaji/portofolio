<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori_artikel extends Model
{
    protected $table = 'kategori_artikels';
    protected $fillable = ['nama_kategori', 'deskripsi_kategori'];

    public function artikels(){
        return $this->hasMany('App\Artikel', 'kategori_id', 'id');
    }
}
