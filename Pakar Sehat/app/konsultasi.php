<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class konsultasi extends Model
{
    protected $table = 'konsultasis';
    protected $fillable = ['judul_konsultasi', 'nama_pasien', 'id_dokter', 'thread_starter'];
    public $timestamps = false;
    public function detail_konsultasi(){
        return $this->hasMany('App\detail_konsultasi', 'id_konsultasi', 'id');
    }
    public function dokters(){
        return $this->belongsTo('App\dokter', 'id_dokter','id');
    }
}
