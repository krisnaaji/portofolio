<?php

namespace App\Http\Controllers;

use App\artikel;
use App\kategori_artikel;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class ArtikelController extends Controller
{
    public function index(){
        $artikel = artikel::get();
        $kategori = kategori_artikel::get();
        return view('artikel', ['artikel'=>$artikel, 'kategori'=>$kategori]);
    }

    public function detail($id){
        $artikel = artikel::where('id',$id)->get();
        return view('detail_artikel', ['artikel'=>$artikel[0]]);
    }

    public function cari(Request $request){
        $cari = $request->search;
        $artikel = artikel::where('judul_artikel','LIKE','%'.$cari.'%')->get();
        $kategori = kategori_artikel::get();
        return view('artikel', ['artikel'=>$artikel, 'kategori'=>$kategori]);

    }
}
