<?php

namespace App\Http\Controllers;

use App\dokter;
use App\dokter_review;
use App\konsultasi;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $dokter = dokter::get();
        $hitung = dokter::with('konsultasi')->get();
        $review = dokter_review::get();
        return view ('profiledokter',['dokter'=>$dokter,'review'=>$review, 'hitung'=>$hitung]);
    }
    public function detail($id){
        return view('detail_profile');
    }
}
