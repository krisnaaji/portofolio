@extends('layouts.artikel_layout')
<!-- <style>
    .sidenav {
        width: 130px;
        position: fixed;
        z-index: 1;
        top: 20%;
        left: 10px;
        background: #eee;
        overflow-x: hidden;
        padding: 8px 0;
    }

    .sidenav a {
        padding: 6px 8px 6px 16px;
        text-decoration: none;
        font-size: 25px;
        color: #2196F3;
        display: block;
    }

    .sidenav a:hover {
        color: #064579;
    }

    .main {
        margin-left: 140px;
        /* Same width as the sidebar + left position in px */
        font-size: 28px;
        /* Increased text to enable scrolling */
        padding: 0px 10px;
    }

    @media screen and (max-height: 450px) {
        .sidenav {
            padding-top: 15px;
        }

        .sidenav a {
            font-size: 18px;
        }
    }
</style> -->
@section('content')

<br>
<br>
<br>
<br>
<!-- <div class="sidenav">
    <p class="h4 text-center">Kategori</p>
    @foreach($kategori as $k)
    <a href="/{{ $k->nama_kategori }}">{{ $k->nama_kategori }}</a>
    @endforeach
</div> -->
<div class="container">

    <h1 class="text-center">Artikel PakarSehat</h1>
    <br>
    <div class="card-deck">
        <div class="card-deck">
            @foreach($artikel as $a)
            <div class="card">
                <img src="img/gambar_artikel/{{ $a->gambar_artikel }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title" style="color: black;">{{ $a->judul_artikel}}</h5>
                    <a class="card-text btn btn-primary" href="/artikel/detail/{{ $a->id }}">Detail Artikel</a>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>
@endsection