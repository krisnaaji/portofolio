@extends('layouts.artikel_layout')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gsap@3.0.2/dist/gsap.min.js"></script>
<style>
    @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700");

    html {
        width: auto;
        height: auto;
    }

    body {
        background-image: linear-gradient(180deg, #ffb253 0%, #f56259 100%);
        width: auto;
        height: auto;
    }

    .slider-wrap {
        height: 100%;
        width: 100%;
    }

    .slider-wrap .slider {
        position: absolute;
        width: 100%;
        left: 40%;
        top: 40%;
    }

    .slider-item {
        width: 530px;
        padding: 20px 0 25px 30px;
        border-radius: 10px;
        background-color: #ffffff;
        display: flex;
        justify-content: flex-start;
        position: absolute;
        opacity: 0;
        z-index: 0;
        box-shadow: 0 4px 9px #f1f1f4;
        position: absolute;
        left: 0;
        top: 0;
    }

    .slider-item .animation-card_image {
        max-width: 60px;
        max-height: 60px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        box-shadow: 0 4px 9px rgba(241, 241, 244, 0.72);
        background-color: #ffffff;
    }

    .slider-item .animation-card_image img {
        width: 53px;
        height: 53px;
        border-radius: 50%;
        object-fit: cover;
    }

    .slider-item .animation-card_content {
        width: 100%;
        max-width: 374px;
        margin-left: 26px;
        font-family: "Open Sans", sans-serif;
    }

    .slider-item .animation-card_content .animation-card_content_title {
        color: #4a4545;
        font-size: 16px;
        font-weight: 400;
        letter-spacing: -0.18px;
        line-height: 24px;
        margin: 0;
    }

    .slider-item .animation-card_content .animation-card_content_description {
        color: #696d74;
        font-size: 15px;
        font-weight: 300;
        letter-spacing: normal;
        line-height: 24px;
        margin: 10px 0 0 0;
    }

    .slider-item .animation-card_content .animation-card_content_city {
        font-size: 11px;
        margin: 10px 0 0 0;
        font-size: 12px;
        font-weight: 500;
        text-transform: uppercase;
        color: #696d74;
    }
</style>

@section('content')

<div class="container">
    <div class="text-center" style="margin-top:10%; font-family: 'Montserrat', sans-serif; font-style:normal; color: black;">
        <h1>Kami Pakar<strong>Sehat</strong></h1>
        <br>
        <h1>Kesehatan Anak adalah Prioritas Kami</h1>
        <br>
        <br>
        <!-- <div class="row">
            <div class="col">
                <p style="font-size:larger">Anak Anak adalah periode pekembangan yang merentang
                    dari masa bayi hingga usia lima atau enam tahun,
                    periode ini biasanya disebut dengan periode prasekolah,
                    kemudian berkembang setara dengan tahun tahun sekolah dasar.
                    <br>
                    <br>
                    disini Kami membantu mengatasi penyakit
                    yang mungkin sering ditemui di masa anak anak
                </p>
            </div>
            <div class="col">
                <img src="\img\svg\undraw_doctor_kw5l.svg" alt="INI GAMBAR" height="300px">
            </div>
        </div> -->
    </div>
    <!-- <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    @foreach($review as $r)    
    <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="{{$r->id}}" class="active"></li>
        </ol>
        <div class="carousel-inner">
            @foreach($pertama as $rr)
        <div class="carousel-item active" data-interval="3000">
                <div class="card">
                    <div class="row">
                        <div class="col">
                        <img src="img/gambar_review/{{$rr->foto}}" class="card-img" alt="..." style="height:150px; border-radius: 50%; width: 150px; ">
                        </div>
                        <div class="col">
                            <p>{{$rr->nama_pasien}}</p>
                            <div class="row">
                                <p>{{$rr->isi}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="carousel-item" data-interval="3000">
                <img src="img/gambar_review/{{$r->foto}}" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5 style="color: white;text-shadow: 0px 4px 4px rgba(0, 0, 0, 1);">Dwi</h5>
                    <p style="color: white;text-shadow: 0px 4px 4px rgba(0, 0, 0, 1);">Terimakasih PakarSehat. Pengetahuan saya mengenai anak=anak lebih meningkat.</p>
                </div>
            </div>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        @endforeach
    </div> -->

    <div class="slider-wrap">
        <div id="card-slider" class="slider">
            @foreach($review as $r)
            <div class="slider-item">
                <div class="animation-card_image">
                    <img src="img/gambar_review/{{$r->foto}}" alt="">
                </div>
                <div class="animation-card_content">
                    <h4 class="animation-card_content_title title-2">{{$r->nama_pasien}}</h4>
                    <p class="animation-card_content_description p-2">{{$r->isi}}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<script>
    var cards = $('#card-slider .slider-item').toArray();

    startAnim(cards);

    function startAnim(array) {
        if (array.length >= 4) {
            TweenMax.fromTo(array[0], 0.5, {
                x: 0,
                y: 0,
                opacity: 0.75
            }, {
                x: 0,
                y: -120,
                opacity: 0,
                zIndex: 0,
                delay: 0.03,
                ease: Cubic.easeInOut,
                onComplete: sortArray(array)
            });

            TweenMax.fromTo(array[1], 0.5, {
                x: 79,
                y: 125,
                opacity: 1,
                zIndex: 1
            }, {
                x: 0,
                y: 0,
                opacity: 0.75,
                zIndex: 0,
                boxShadow: '-5px 8px 8px 0 rgba(82,89,129,0.05)',
                ease: Cubic.easeInOut
            });

            TweenMax.to(array[2], 0.5, {
                bezier: [{
                    x: 0,
                    y: 250
                }, {
                    x: 65,
                    y: 200
                }, {
                    x: 79,
                    y: 125
                }],
                boxShadow: '-5px 8px 8px 0 rgba(82,89,129,0.05)',
                zIndex: 1,
                opacity: 1,
                ease: Cubic.easeInOut
            });

            TweenMax.fromTo(array[3], 0.5, {
                x: 0,
                y: 400,
                opacity: 0,
                zIndex: 0
            }, {
                x: 0,
                y: 250,
                opacity: 0.75,
                zIndex: 0,
                ease: Cubic.easeInOut
            }, );
        } else {
            $('#card-slider').append('<p>Sorry, carousel should contain more than 3 slides</p>')
        }
    }

    function sortArray(array) {
        clearTimeout(delay);
        var delay = setTimeout(function() {
            var firstElem = array.shift();
            array.push(firstElem);
            return startAnim(array);
        }, 3000)
    }
</script>
@endsection