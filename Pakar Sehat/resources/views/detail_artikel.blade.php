@extends('layouts.artikel_layout')
<script>
    function goBack() {
        window.history.back();
    }
</script>
@section('content')
<br><br><br>
<br>
<div class="container">
<button onclick="goBack()" class="btn btn-primary">Kembali</button>
<h1>{{$artikel->judul_artikel}}</h1>
<br>
<img src="/img/gambar_artikel/{{$artikel->gambar_artikel}}" alt="">
<br>
<br>
<?php
    echo "{$artikel->detail_artikel}";
?>

</div>

@endsection