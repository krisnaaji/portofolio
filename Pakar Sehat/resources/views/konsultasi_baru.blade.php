@extends('layouts.artikel_layout')

@section('content')
<br><br><br><br>
<div class="container">
    @if(count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }} <br />
        @endforeach
    </div>
    @endif
    <div class="form-group">
        @foreach($dokter as $d)
        <form action="/konsultasi/upload" method="post">
            {{ csrf_field() }}
            <h1>Konsultasi baru dengan dokter {{$d->nama_dokter}} </h1>
            <input type="text" name="dokterID" value="{{$d->id}}" hidden="true">
            <label>Nama Kamu</label>
            <input type="text" name="nama" class="form-control">
            <br>
            <label>Topik Konsultasi</label>
            <input type="text" name="topic" class="form-control">
            <br>
            <label>Deskripsikan konsultasimu</label>
            <textarea name="desc" class="form-control"></textarea>
            <br>
            <br>
            <input type="submit" class="btn btn-primary" value="Kirim Konsultasi">
        </form>
        @endforeach
    </div>
</div>
@endsection